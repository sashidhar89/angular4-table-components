

Angular 4 Table  is a angular table component . It currently has these features supported

  The component should display Sample Data in a table - Done

§  User should be able to select how many rows are displayed in the table - Done

§  Table should be paginated if not all rows are displayed on the screen based on the user’s selection

§  Pagination options should be displayed in the table footer

§  Column names should be displayed in the table header

§  Entire table, table header and table footer should always be displayed on the screen while scrolling

§  If number of rows exceeds the size of the table body, a vertical scrollbar should be displayed within the table body – only table body shall scroll vertically, table header and footer shall remain as is

§  If number of columns exceed the size of the table body, a horizontal scrollbar should be displayed within the table body – only table body and table header shall scroll to reveal the additional columns, table footer shall remain as is